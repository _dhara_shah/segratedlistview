package dhara.example.com.segratedlistview.manager;

import java.util.ArrayList;
import java.util.List;

import dhara.example.com.segratedlistview.model.Hotel;

/**
 * Created by user on 18/12/2015.
 */
public class StaticManager {
    private static StaticManager mManager;

    /**
     * Gets the instance of StaticManager </br>
     * Using the SingleTon method
     * @return
     */
    public static StaticManager getInstance() {
        if(mManager == null) {
            mManager = new StaticManager();
        }
        return mManager;
    }

    /**
     * Creates a set of 20 static hotels
     * and returns the same
     * @return
     */
    public List<Hotel> getHotelList() {
        List<Hotel> hotelList = new ArrayList<>();

        for(int i=0;i<20;i++) {
            Hotel hotel = new Hotel();
            hotel.setDescription(":app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources :app:generateDebugSources");
            hotel.setHotelName("Hotel " + (i+1));
            hotelList.add(hotel);
        }
        return hotelList;
    }
}
