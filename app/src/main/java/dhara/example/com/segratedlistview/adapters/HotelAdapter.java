package dhara.example.com.segratedlistview.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dhara.example.com.segratedlistview.R;
import dhara.example.com.segratedlistview.model.Hotel;
import dhara.example.com.segratedlistview.model.StaticHolder;

/**
 * Created by user on 18/12/2015.
 */
public class HotelAdapter extends BaseAdapter {
    List<Hotel> mHotelList;
    Context mContext;
    int RESOURCE;

    List<Object> mObjList;

    public HotelAdapter(Context context, int resource, List<Hotel> objects) {
        mContext = context;
        RESOURCE = resource;
        mHotelList = objects;

        mObjList = new ArrayList<>();

        // we perform this operation to add separators between the hotel lists
        // that is after every 4 elements. This might not be the correct approach
        // if at all there are 1000 records to deal with but in that case
        // we can ensure that the objects would be similar in nature with may be a
        // flag to differentiate one from the other

        // reference from : <a href="http://android.amberfog.com/?p=296">http://android.amberfog.com/?p=296</a>
        for(int i=0;i<mHotelList.size();i++) {
            if((i+1) % 5 == 0) {
                mObjList.add(i,new StaticHolder());
            }
            mObjList.add(mHotelList.get(i));
        }
    }

    @Override
    public int getCount() {
        return mObjList.size();
    }

    @Override
    public Object getItem(int i) {
        return mObjList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder.RowTypeOne rowTypeOneHolder = null;
        ViewHolder.RowTypeTwo rowTypeTwoHolder = null;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // checks the type of view that should be returned
            switch (getItemViewType(position)) {
                // the first view kind
                case 0:
                    convertView = inflater.inflate(R.layout.individual_hotel_row, parent, false);
                    rowTypeOneHolder = new ViewHolder.RowTypeOne();
                    rowTypeOneHolder.txtDesc = (TextView)convertView.findViewById(R.id.txtDesc);
                    rowTypeOneHolder.txtHotelName = (TextView)convertView.findViewById(R.id.txtHotelName);
                    convertView.setTag(rowTypeOneHolder);
                    break;

                // the second view kind
                case 1:
                    convertView = inflater.inflate(R.layout.divider_layout, parent, false);
                    rowTypeTwoHolder = new ViewHolder.RowTypeTwo();
                    rowTypeTwoHolder.txtOffers = (TextView)convertView.findViewById(R.id.txtOffers);
                    convertView.setTag(rowTypeTwoHolder);
                    break;
            }
        }else {
            switch (getItemViewType(position)) {
                case 0:
                    rowTypeOneHolder = (ViewHolder.RowTypeOne) convertView.getTag();
                    break;

                case 1:
                    rowTypeTwoHolder = (ViewHolder.RowTypeTwo) convertView.getTag();
                    break;
            }
        }

        // checks the instance of the object to ensure that
        // the correct data is being fetched and displayed
        if(mObjList.get(position) instanceof StaticHolder &&
                getItemViewType(position) == 1) {
            // do nothing
        }else {
            // set data here
            Hotel hotel = (Hotel)mObjList.get(position);
            rowTypeOneHolder.txtDesc.setText(hotel.getDescription());
            rowTypeOneHolder.txtHotelName.setText(hotel.getHotelName());
        }

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        // we check that the position will lead
        // to the second type of view or not
        // should always begin from 0 in order to avoid
        // ArrayIndexOutOfBounds
        // at {android.widget.AbsListView$RecycleBin.addScrapView(AbsListView.java:6739)
        if((position+1) % 5 == 0) {
            return 1;
        }else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        // by default 1 is returned ,
        // this can be modified to suit our needs
        return 2;
    }

    /**
     * ViewHolder pattern to avoid multiple generation of views
     * and enable recycling of views to take place
     */
    static class ViewHolder {
        /**
         * For the first type of rows
         */
        static class RowTypeOne {
            TextView txtHotelName;
            TextView txtDesc;
        }

        /**
         * For the second type of rows
         */
        static class RowTypeTwo {
            TextView txtOffers;
        }
    }
}
