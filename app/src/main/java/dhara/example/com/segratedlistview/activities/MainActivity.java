package dhara.example.com.segratedlistview.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

import dhara.example.com.segratedlistview.R;
import dhara.example.com.segratedlistview.adapters.HotelAdapter;
import dhara.example.com.segratedlistview.manager.StaticManager;
import dhara.example.com.segratedlistview.model.Hotel;


public class MainActivity extends AppCompatActivity {
    ListView mListView;
    HotelAdapter mAdapter;
    private List<Hotel> mHotelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = (ListView)findViewById(android.R.id.list);
        mHotelList = StaticManager.getInstance().getHotelList();
        mAdapter = new HotelAdapter(this, 0, mHotelList);
        mListView.setAdapter(mAdapter);
    }
}
