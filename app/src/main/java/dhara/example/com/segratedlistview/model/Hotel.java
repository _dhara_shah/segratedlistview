package dhara.example.com.segratedlistview.model;

/**
 * Created by user on 18/12/2015.
 */
public class Hotel {
    private String hotelName;
    private String description;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
