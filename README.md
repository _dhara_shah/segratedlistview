# README #

### What is this repository for? ###

* This application is just a demo to demonstrate the use of getViewTypeCount and getViewType
* Version

### How do I get set up? ###

* Android studio is used to code this application.
* Import this application or simply open build.gradle using android studio. Run the application like every normal app, and obtain the apk from app/build/output/apk/ folder.
